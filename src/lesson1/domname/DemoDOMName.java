/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson1.domname;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author viquy
 */
public class DemoDOMName {
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("Students.xml");
            NodeList student = document.getElementsByTagName("member");
//            NodeList elementsByTagName = document.getElementsByTagName("member");

//            for (int i = 0; i < elementsByTagName.getLength(); i++) {
//                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
//                for (int j = 0; j < childNodes.getLength(); j++) {
//                    Node item = childNodes.item(j);
//                    if (item.getNodeName() != null && item.getNodeName().equals("username")) {
//                        System.out.println(childNodes.item(j).getTextContent());
//                    }
//                }
//            }
            int count= 0;
            for(int i =0; i< student.getLength();i++){
                count++;
                System.out.println(count);
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DemoDOMName.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(DemoDOMName.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DemoDOMName.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
