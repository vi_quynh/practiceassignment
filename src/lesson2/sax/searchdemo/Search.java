/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesson2.sax.searchdemo;

import java.util.LinkedList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author viquy
 */
public class Search extends DefaultHandler{
    public String memberID;
    public String memberName;
    public List<String> listmember = new LinkedList<String>();

    @Override
    public void startDocument() throws SAXException {
        
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        memberID = attributes.getValue("id");
        memberName = attributes.getValue("name");
        listmember.add(memberID);
        listmember.add(memberName);
        
    }
    public List<String> getList(){
        return  listmember;
    }
    public List<String> getListbyName(String name){
        List<String> resultList = new LinkedList<String>() ;
        for (String item : listmember ) {
            if (item.contains(name)) {
                resultList.add(item);
            }
        }
        return resultList;
    }
    
    
    
    
}
