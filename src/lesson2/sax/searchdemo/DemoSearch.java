/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesson2.sax.searchdemo;

import java.io.File;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author viquy
 */
public class DemoSearch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            Search s = new Search();
            parser.parse(new File("demo.xml"), s);
            System.out.println("All member");
            System.out.println(s.getList());
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter membername :");
            String membername = scanner.next();
            System.out.println("Result");
            System.out.println(s.getListbyName(membername));
        } catch (Exception ex) {
            Logger.getLogger(DemoSearch.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
}
