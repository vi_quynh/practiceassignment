/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesson2.sax.countnumber;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author viquy
 */
public class CountNumberOfTags {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            
            CounterNumberOfTagHandler handler = new CounterNumberOfTagHandler();
            
            parser.parse(new File("tomcat-users.xml"), handler);
            
            System.out.println(handler.getCount());
        } catch (Exception ex) {
            Logger.getLogger(CountNumberOfTags.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
