/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesson2.sax.countnumber;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author viquy
 */
public class CounterNumberOfTagHandler extends DefaultHandler{
    private int count;

    @Override
    public void startDocument() throws SAXException {
        count = 0;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        count++;
    }
    
    
    @Override
    public void endDocument() throws SAXException {
        
    }

    public int getCount() {
        return count;
    }
    
    
    
}
