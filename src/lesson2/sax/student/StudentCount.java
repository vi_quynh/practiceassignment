/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson2.sax.student;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 *
 * @author DELL1450
 */
public class StudentCount {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            Student s = new Student();
            parser.parse(new File("Students.xml"), s);
            System.out.println(s.getList());
        } catch (Exception ex) {
            Logger.getLogger(StudentCount.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
