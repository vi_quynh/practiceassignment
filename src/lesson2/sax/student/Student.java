/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson2.sax.student;

import java.util.LinkedList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author DELL1450
 */
public class Student extends DefaultHandler{
    public String studentID;
    public String studentName;
    List<String> listOfStudent = new LinkedList<>();

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        studentID = attributes.getValue("id");
        studentName = attributes.getValue("name");
        listOfStudent.add(studentID);
        listOfStudent.add(studentName);
    }

    @Override
    public void endDocument() throws SAXException {
    }

    public List<String> getList() {
        return listOfStudent;
    }

}
